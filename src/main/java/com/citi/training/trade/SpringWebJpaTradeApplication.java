package com.citi.training.trade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebJpaTradeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebJpaTradeApplication.class, args);
	}

}
