package com.citi.training.trade.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trade.model.Trade;

/**
 * @author Stephen
 * 
 * <p> This is the DAO we are using the Crud Repo to interact with the DB <\p>
 * 
 */

public interface TradeDao extends CrudRepository<Trade, Long> {}