package com.citi.training.trade.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.model.Trade;
import com.citi.training.trade.rest.TradeController;


/**
 * @author Stephen
 * 
 * <p> Creating trades endpoint get, delete and post requests. <\p>
 * 
 *
 * 
 */

@RestController
@RequestMapping("/trades")
public class TradeController  {
	
    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
    
    @Autowired
    private TradeDao tradeDao;
    
    	/**
	 * Get all trade
	 * @param Trade 
	 * @return An iterable list of Trades
	 */
    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Trade> findAll(){
    	LOG.info("HTTP GET REQUEST CALLING findAll(");
    	return tradeDao.findAll();
    }
    
    	/**
	 * Gets trade by ID
	 * @param Trade 
	 * @return the trade
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Trade findById(@PathVariable long id) {
        LOG.info("HTTP GET findById() id=[" + id + "]");
        return tradeDao.findById(id).get();
    }
    
    	/**
	 * Saves trade to the DB
	 * @param Trade 
	 * @return REST responce code to tell you its created
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.POST)
    public HttpEntity<Trade> save(@RequestBody Trade trade){
    	LOG.info("HTTP POST REQUEST CALLING save()=[" + trade + "]");
    	return new ResponseEntity<Trade>(tradeDao.save(trade), HttpStatus.CREATED);
    }
    
    	/**
	 * Deletes a trade via the ID
	 * @param Trade 
	 * @return Returns a 204 okay with no body 
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        tradeDao.deleteById(id);
    }
}
