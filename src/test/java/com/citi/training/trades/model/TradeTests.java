package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;



import org.junit.Test;

import com.citi.training.trade.model.Trade;

public class TradeTests {

	
    private int testId = 20;
    private String testName = "APPL";
    private double testPrice = 10.50;
    private int testVolume = 4;
    private int testBuy = 0;

    @Test
    public void test_Trade_defaultConstructorAndSetters() {
        Trade testTrade = new Trade();

        testTrade.setBuy(testBuy);
        testTrade.setId(testId);
        testTrade.setPrice(testPrice);
        testTrade.setVolume(testVolume);
        testTrade.setStock(testName);


        assertEquals("Buy",
        		testBuy, testTrade.getBuy());
        assertEquals("Name",
                     testName, testTrade.getStock());
        assertEquals("Vol",
        		testVolume, testTrade.getVolume());
        assertEquals("Id",
                testId, testTrade.getId());
        assertEquals("Price",
        		testPrice, testTrade.getPrice(), 0.00001);
    }

    
    @Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(testId, testBuy, testPrice, testName, testVolume);

        assertEquals("Buy",
        		testBuy, testTrade.getBuy());
        assertEquals("Name",
                     testName, testTrade.getStock());
        assertEquals("Vol",
        		testVolume, testTrade.getVolume());
        assertEquals("Id",
                testId, testTrade.getId());
        assertEquals("Price",
        		testPrice, testTrade.getPrice(), 0.00001);

    }

    @Test

        //public Trade(long id, int buy, double price, String stock, int volume  ) {
	public void test_Trade_toString() {
		Trade testTrade = new Trade(testId,testBuy, testPrice, testName, testVolume);
		String expectedString = "{\"id\":" + testId + ",\"stock\":\""+ testName + "\",\"buy\":"+ testBuy + ",\"price\":"+ testPrice +
							  ",\"volume\":"+ testVolume +"}";
		assertEquals("Testing to string", expectedString, testTrade.toString());
                    
		
	}


}
