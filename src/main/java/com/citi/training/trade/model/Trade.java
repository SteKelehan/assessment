package com.citi.training.trade.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Stephen
 * 
 * DB object. In this case stocks.
 * 
 */
@Entity
public class Trade {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length=10)
	private String stock;
	private int buy;
	private double price;
	private int volume;
	
	public Trade() {}

	public Trade(long id, int buy, double price, String stock, int volume  ) {
		this.id = id;
		this.stock = stock;
		this.buy = buy;
		this.price = price;
		this.volume = volume;
	}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getVolume() {
		return volume;
	}
	
	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getBuy() {
		return buy;
	}

	public void setBuy(int buy) {
		this.buy = buy;
	}
	
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public String getStock() {
		return stock;
	}
	
	
	@Override
	public String toString() {
		return "{\"id\":" + this.id + ",\"stock\":\""+ this.stock + "\",\"buy\":"+ this.buy + ",\"price\":"+ this.price + 
		",\"volume\":"+ this.volume +"}";
	}


}
